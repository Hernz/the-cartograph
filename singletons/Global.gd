extends Node

#Gold
var gold = 0 setget gold_set, gold_get

var torchActive = false setget torch_set, torch_get

var difficulty = ""

signal gold_changed
signal torch_changed

enum {UNRANKED, S, A, B ,C}
export var colorRankS : Color;
export var colorRankA : Color;
export var colorRankB : Color;
export var colorRankC : Color;
var lastRank = {"Easy":UNRANKED,"Medium":UNRANKED,"Hard":UNRANKED}

func getRankAsString(d):
	if lastRank[d]==UNRANKED:
		return ""
	elif lastRank[d]==C:
		return "C"
	elif lastRank[d]==B:
		return "B"
	elif lastRank[d]==A:
		return "A"
	elif lastRank[d]==S:
		return "S"
		
func getRankColor(d):
	if lastRank[d]==UNRANKED:
		return Color.black
	elif lastRank[d]==C:
		return colorRankC
	elif lastRank[d]==B:
		return colorRankB
	elif lastRank[d]==A:
		return colorRankA
	elif lastRank[d]==S:
		return colorRankS

func getRankValue(d):
	if lastRank[d]==UNRANKED:
		return 0
	elif lastRank[d]==C:
		return 1
	elif lastRank[d]==B:
		return 2
	elif lastRank[d]==A:
		return 3
	elif lastRank[d]==S:
		return 4

#Items
var items : Dictionary  = {"torch" : 1}

signal items_changed

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func _process(delta: float) -> void:
	#gold_set(gold+1)
	pass
	
func gold_set(v):
	gold = v
	emit_signal("gold_changed")
	
func gold_get():
	return gold
	
func torch_set(v):
	torchActive = v
	emit_signal("torch_changed")
	
func torch_get():
	return torchActive
	
func add_item(var name : String):
	var v = 0
	if items.has(name):
		v = items[name]
	items[name] = v + 1
	emit_signal("items_changed")
	
func remove_item(var name : String):
	if !items.has(name) || items[name]<1:
			return
	items[name] = items[name] - 1
	emit_signal("items_changed")
	
func get_total_item(var name : String):
	if !items.has(name):
		return 0
	return items[name]
