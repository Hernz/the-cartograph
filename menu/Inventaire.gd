extends Control

onready var itemLine = preload("res://menu/InventoryItemLine.tscn")


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	updateInventory()
	pass # Replace with function body.


func _enter_tree():
	Global.connect("items_changed",self,"updateInventory")

func _exit_tree():
	Global.disconnect("items_changed",self,"updateInventory")

func updateInventory():
	for n in $Panel/VBox.get_children():
		$Panel/VBox.remove_child(n)
		n.queue_free()
	for k in Global.items:
		var v =  Global.items[k] 
		if v>0:
			var il = itemLine.instance()
			var nom = ""
			if(k=="clepsydre"):
				nom = "Hourglass"
			elif(k=="masterkey"):
				nom = "Cryptic key"
			elif(k=="torch"):
				nom = "Torch"
			il.get_node("nom").text = nom
			il.get_node("quantite").text = "("+str(v)+")"
			$Panel/VBox.add_child(il)
	pass
