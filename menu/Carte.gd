extends Control

export var difficulty : String
onready var music = get_tree().get_root().find_node("AudioStreamPlayer",true,false)
onready var gs = get_tree().get_root().find_node("GuiSound",true,false)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("mouse_entered",self,"highlight")
	connect("mouse_exited",self,"dehighlight")
	connect("gui_input",self,"onClick")
	if Global.lastRank[difficulty] != Global.UNRANKED:
		$LastRank.text = Global.getRankAsString(difficulty)
		$LastRank.modulate = Global.getRankColor(difficulty)
		$LastRank.visible = true
	else:
		$LastRank.visible = false
	pass # Replace with function body.

func highlight():
	$door/AnimationPlayer.play("Hover")
	gs.play()
	self.get_parent().get_parent().find_node("difficulty").text = difficulty
	
func dehighlight():
	$door/AnimationPlayer.play("Normal")
	self.get_parent().get_parent().find_node("difficulty").text = ""
	
func onClick(var e : InputEvent):
	if(e.is_action_pressed("ui_select")):
		music.stop()
		music.queue_free()
		yield(get_tree().create_timer(.2), "timeout")
		get_tree().current_scene.queue_free()
		Global.difficulty = difficulty
		get_tree().change_scene("res://Game.tscn")
