extends PanelContainer

export var prix : int = 10
export var nameInGlobalSingleton : String = ""

onready var barre = get_parent().get_parent() #Barre (Ah ah ah !)
onready var shop = get_tree().get_root().find_node("Shop",true,false)

var selected = false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	assert(shop)
	deselect()
	dehover()
	connect("mouse_entered",self,"hover")
	connect("mouse_exited",self,"dehover")
	connect("gui_input",self,"onClick")
	$IconNomPrix/Prix.text = str(prix)

func _enter_tree():
	Global.connect("gold_changed",self,"updateBuyButtonVisibilty")

func _exit_tree():
	Global.disconnect("gold_changed",self,"updateBuyButtonVisibilty")

func updateBuyButtonVisibilty():
	if(Global.gold>=prix):
		self.modulate = Color(1, 1, 1, 1)
	else:
		self.modulate = Color(1, 1, 1, .5)

func canBeBuy():
	return Global.gold>=prix

func add_item():
	if(canBeBuy()):
		Global.gold -= prix
		Global.add_item(nameInGlobalSingleton)
		$BuySound.play()
	pass
	
func deselect():
	material.set_shader_param("thickness",0)
	selected = false
	
func hover():
	if !selected:
		material.set_shader_param("thickness",2)
		shop._select(self)
		$GuiSound.play()
	
func dehover():
	if !selected:
		material.set_shader_param("thickness",0)
		shop._select(self)

func select():
	selected = true
	material.set_shader_param("thickness",4)
		
func onClick(var e : InputEvent):
	if(e.is_action_pressed("ui_select")):
		if(canBeBuy()):
			add_item()
