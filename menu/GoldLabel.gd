extends Label


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	self.text = str(Global.gold)
	
func _enter_tree():
	Global.connect("gold_changed",self,"setGoldText")

func _exit_tree():
	Global.disconnect("gold_changed",self,"setGoldText")

func setGoldText():
	self.text = str(Global.gold)
