extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


func _select(var item):
	if(!item):
		return
	for c in $Items.get_children():
		c.deselect()
	item.select()
	$Description.visible = true
	$Description/Control/RichTextLabel.text = item.get_node("Description").text
