extends Node2D

onready var game = get_node("/root/Game")
onready var player = game.find_node("Player")

var current_map

func _ready():
	player.connect("goal_reached",self,"_on_player_goal_reached")
	game.connect("map_loaded",self,"_on_new_map_loaded")
	Global.connect("torch_changed",self,"on_torch_changed")

func _process(delta: float) -> void:
	if(player.move_to_tile):
		clear()
		update()#TODO: optimiser

func clear():
	find_node("BackGround").clear()
	find_node("Objects").clear()

func on_torch_changed():
	clear()
	update()

func update():
	if current_map == null:
		return
	var player_map_position = find_node("BackGround").world_to_map(player.position)
	var pos
	for direction in [Vector2.UP,Vector2.DOWN,Vector2.LEFT,Vector2.RIGHT,Vector2.ZERO]:
		pos = player_map_position+direction
		find_node("BackGround").set_cellv(pos,current_map.find_node("BackGround").get_cellv(pos),current_map.find_node("BackGround").is_cell_x_flipped(pos.x,pos.y),current_map.find_node("BackGround").is_cell_y_flipped(pos.x,pos.y),current_map.find_node("BackGround").is_cell_transposed(pos.x,pos.y))
		find_node("Objects").set_cellv(pos,current_map.find_node("Objects").get_cellv(pos),current_map.find_node("Objects").is_cell_x_flipped(pos.x,pos.y),current_map.find_node("Objects").is_cell_y_flipped(pos.x,pos.y),current_map.find_node("Objects").is_cell_transposed(pos.x,pos.y))
	if(Global.torchActive):
		for direction in [Vector2.UP*2,Vector2.DOWN*2,Vector2.LEFT*2,Vector2.RIGHT*2,Vector2(1,1),Vector2(-1,1),Vector2(1,-1),Vector2(-1,-1)]:
			pos = player_map_position+direction
			find_node("BackGround").set_cellv(pos,current_map.find_node("BackGround").get_cellv(pos),current_map.find_node("BackGround").is_cell_x_flipped(pos.x,pos.y),current_map.find_node("BackGround").is_cell_y_flipped(pos.x,pos.y),current_map.find_node("BackGround").is_cell_transposed(pos.x,pos.y))
			find_node("Objects").set_cellv(pos,current_map.find_node("Objects").get_cellv(pos),current_map.find_node("Objects").is_cell_x_flipped(pos.x,pos.y),current_map.find_node("Objects").is_cell_y_flipped(pos.x,pos.y),current_map.find_node("Objects").is_cell_transposed(pos.x,pos.y))

func _on_new_map_loaded(new_map):
	clear()
	current_map = new_map
	update()
	#var event_tile : Vector2 = find_node("BackGround").world_to_map(player.position)
	#if find_node("BackGround").get_cellv(event_tile) != -1 and find_node("Objects").get_cellv(event_tile) == -1:
	#	player.move_to(find_node("BackGround").map_to_world(event_tile))

func _on_player_goal_reached():
	clear()
	update()

func _input(event):
	if event.is_action_pressed("ui_select") && !player.move_to_tile:
		var position_tile : Vector2 = find_node("BackGround").world_to_map(find_node("BackGround").to_local(player.to_global(Vector2.ZERO)))
		var event_tile : Vector2 = find_node("BackGround").world_to_map(find_node("BackGround").to_local(get_global_mouse_position()))
		if find_node("BackGround").get_cellv(event_tile) != -1 and find_node("BackGround").get_cellv(event_tile)!=find_node("BackGround").tile_set.find_tile_by_name("wall") and find_node("Objects").get_cellv(event_tile) == -1:
			var check_tile = (find_node("BackGround").map_to_world(event_tile)+find_node("BackGround").map_to_world(position_tile)+Vector2.DOWN*10)/2.0
			var check_tile2 = (find_node("BackGround").map_to_world(event_tile)+find_node("BackGround").map_to_world(position_tile)+Vector2.DOWN*50)/2.0
			check_tile = find_node("BackGround").world_to_map(check_tile)
			check_tile2 = find_node("BackGround").world_to_map(check_tile2)
			var b1 = find_node("BackGround").get_cellv(check_tile) != -1 and find_node("BackGround").get_cellv(check_tile)!=find_node("BackGround").tile_set.find_tile_by_name("wall") and find_node("Objects").get_cellv(check_tile) == -1
			var b2 = find_node("BackGround").get_cellv(check_tile2) != -1 and find_node("BackGround").get_cellv(check_tile2)!=find_node("BackGround").tile_set.find_tile_by_name("wall") and find_node("Objects").get_cellv(check_tile2) == -1
			if (b1||b2):
				player.move_to(find_node("BackGround").map_to_world(event_tile))
