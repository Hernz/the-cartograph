extends CanvasLayer

onready var hypotheses : Hypotheses = get_node("/root").find_node("Hypotheses",true,false)
onready var music : AudioStreamPlayer = get_node("/root").find_node("AudioStreamPlayer",true,false)
onready var plateau  = get_node("/root").find_node("PlateauMap",true,false)

export var goldPerChest : int = 150
export var goldPerTrap : int = 50
export var goldPerBearTrap : int = 75
export var goldPerGate : int = 100
export var errors_malus : int = 50

var can_next = false

func _on_game_end():
	can_next = false
	music.queue_free()
	plateau.queue_free()
	get_tree().paused = true
	Global.torchActive = false
	set_scores()
	$Control.show()
	$AnimationPlayer.connect("animation_finished",self,"_on_animation_finished")
	$AnimationPlayer.play("LabelTrapsAnim")
	$Control/NextButton.connect("pressed",self,"goToMenu")

func _on_animation_finished(a):
	can_next = true

func set_scores():
	var rep = hypotheses.get_report()
	#Simple Traps
	var goldTraps = rep["trapFound"]*goldPerTrap
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer/LabelTraps/Score.text = str(rep["trapFound"])+"/"+str(rep["totalTrap"])
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer/LabelTraps/Gold.text = "- "+str(goldTraps)+" Gold"
	
	#Bear Traps
	var goldBearTraps = rep["beartrapFound"]*goldPerBearTrap
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer/LabelBearTrap/Score.text = str(rep["beartrapFound"])+"/"+str(rep["totalbearTrap"])
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer/LabelBearTrap/Gold.text = "- "+str(goldBearTraps)+" Gold"
	
	#Chest
	var goldChest = rep["chestFound"]*goldPerChest
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer2/LabelChess/Score.text = str(rep["chestFound"])+"/"+str(rep["totalChest"])
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer2/LabelChess/Gold.text = "- "+str(goldChest)+" Gold"
	
	# Gate
	var goldGate = rep["gateFound"]*goldPerGate
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer2/LabelGate/Score.text = str(rep["gateFound"])+"/"+str(rep["totalGate"])
	$Control/VBoxContainer/VBoxContainer3/VBoxContainer2/LabelGate/Gold.text = "- "+str(goldGate)+" Gold"
	
	#Total
	var total = goldTraps + goldChest + goldBearTraps + goldGate
	$Control/VBoxContainer/LabelGold/Gold.text = str(total)
	
	#Errors
	var errors = rep["errors"]
	var fixed_total = total-errors*errors_malus if total-errors*errors_malus >=0 else 0
	$Control/VBoxContainer/LabelGold/Errors.text = " - "+str(errors*errors_malus)+" ("+str(errors)+" errors) = "
	$Control/VBoxContainer/LabelGold/TotalGold.text = str(fixed_total)+" Gold"
	
	#Rank
	var rankRatio = (rep["trapFound"]+rep["chestFound"]+rep["beartrapFound"]+rep["gateFound"])/float((rep["totalTrap"]+rep["totalChest"]+rep["totalbearTrap"]+rep["totalGate"]))
	print(rankRatio)
	#print(rankRatio)
	var rankText = "S"
	var rankColor = Global.colorRankS
	if(rankRatio<=0.3):
		rankText = "C"
		rankColor = Global.colorRankC
	elif(rankRatio<=0.6):
		rankText = "B"
		rankColor = Global.colorRankB
	elif(rankRatio<=0.9):
		rankText = "A"
		rankColor = Global.colorRankA
	
	if rankText == "S" and rep["errors"] > 0:
		rankText = "A"
		rankColor = Global.colorRankA

	if rankText == "A" and rep["errors"] > 1:
		rankText = "B"
		rankColor = Global.colorRankB

	if rankText == "B" and rep["errors"] > 2:
		rankText = "C"
		rankColor = Global.colorRankC
			
	if rankText == "S" and Global.getRankValue(Global.difficulty)<4:
		Global.lastRank[Global.difficulty] = Global.S
	if rankText == "A" and Global.getRankValue(Global.difficulty)<3:
		Global.lastRank[Global.difficulty] = Global.A
	if rankText == "B" and Global.getRankValue(Global.difficulty)<2:
		Global.lastRank[Global.difficulty] = Global.B
	if rankText == "C" and Global.getRankValue(Global.difficulty)<1:
		Global.lastRank[Global.difficulty] = Global.C
		
	$Control/VBoxContainer/LabelRank/Rank.text = rankText
	$Control/VBoxContainer/LabelRank/Rank.self_modulate = rankColor
	Global.gold_set(Global.gold+total)
		

func goToMenu():
	if can_next:
		get_tree().paused = false
		get_tree().change_scene("res://menu/Menu.tscn")
	else:
		$AnimationPlayer.seek(7.6)
		can_next = true
