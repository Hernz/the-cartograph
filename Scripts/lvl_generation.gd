extends Node2D

export var neighbours_threshold : int = 2

export var barrel_number : int = 4
export var trap_number : int = 3
export var beartrap_number : int = 2
export var chest_number : int = 2
export var door_number : int = 2

onready var background : TileMap = $BackGround
onready var objects : TileMap = $Objects
onready var ground_tile : int = background.tile_set.find_tile_by_name("ground")
onready var wall_tile : int = background.tile_set.find_tile_by_name("wall")
onready var chest_tile : int = objects.tile_set.find_tile_by_name("chest")
onready var barrel_tile : int = objects.tile_set.find_tile_by_name("barrel")
onready var trap_tile : int = objects.tile_set.find_tile_by_name("trap_object")
onready var beartrap_tile : int = objects.tile_set.find_tile_by_name("beartrap")
onready var gate_tile : int = objects.tile_set.find_tile_by_name("gate")

onready var door_placement : int = $GatePlacements.tile_set.find_tile_by_name("door_placement")
onready var door_placement_flipped : int = $GatePlacements.tile_set.find_tile_by_name("door_placement_flipped")

var rng = RandomNumberGenerator.new()

func _ready():
	generate()

func get_ground_cells():
	var res : Array = []
	for cell in background.get_used_cells():
		if background.get_cellv(cell) == ground_tile:
			res.append(cell)
	return res

func get_free_cells():
	var res : Array = []
	for cell in background.get_used_cells():
		if background.get_cellv(cell) == ground_tile and objects.get_cellv(cell) == -1:
			res.append(cell)
	return res

func get_objects():
	var res : Array = []
	for i in barrel_number:
		res.append(barrel_tile)
	for j in trap_number:
		res.append(trap_tile)
	for j in beartrap_number:
		res.append(beartrap_tile)
	for j in chest_number:
		res.append(chest_tile)
	
	return res

func get_neighbours_number(available_cells, candidate):
	var neighbours = 0
	for offset in [Vector2.UP,Vector2.DOWN,Vector2.LEFT,Vector2.RIGHT,Vector2(1,1),Vector2(-1,-1),Vector2(-1,1),Vector2(1,-1)]:
		if  (available_cells[candidate]+offset in objects.get_used_cells()):
			neighbours += 1
	return neighbours

func get_neighbours(available_cells, candidate):
	var neighbours : Array = []
	for offset in [Vector2.UP,Vector2.DOWN,Vector2.LEFT,Vector2.RIGHT,Vector2(1,1),Vector2(-1,-1),Vector2(-1,1),Vector2(1,-1)]:
		neighbours.append(available_cells[candidate]+offset)
	return neighbours

func generate():
	randomize()
	objects.clear()
	var candidate : int
	var neighbours_number : int
	
	#place doors
	var door_placements : Array = $GatePlacements.get_used_cells()
	for door in door_number:
		rng.randomize()
		candidate = rng.randi_range(0,door_placements.size()-1)
		if $GatePlacements.get_cellv(door_placements[candidate]) == door_placement_flipped:
			objects.set_cell(door_placements[candidate].x,door_placements[candidate].y,gate_tile,true)
		else:
			objects.set_cell(door_placements[candidate].x,door_placements[candidate].y,gate_tile)
		var neighbours = get_neighbours(door_placements, candidate)
		for neighbor in neighbours:
			var id = door_placements.find(neighbor)
			if id != -1:
				door_placements.remove(id)
		door_placements.remove(candidate)
	# place the rest
	var available_cells : Array = get_ground_cells()
	available_cells.shuffle()
	for object_tile in get_objects():
		rng.randomize()
		candidate = rng.randi_range(0,available_cells.size()-1)
		while available_cells.size() > 0 and get_neighbours_number(available_cells,candidate) > neighbours_threshold:
			available_cells.remove(candidate)
			candidate = rng.randi_range(0,available_cells.size()-1)
		objects.set_cell(available_cells[candidate].x,available_cells[candidate].y,object_tile)
		available_cells.remove(candidate)
