extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

var timer : Timer = null
onready var game : Game = get_node("/root/Game")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	timer = Timer.new()
	timer.connect("timeout",self,"_on_timer_timeout")
	add_child(timer)
	$torch/Label.visible = false
	setTorchText()
	setClepsydreText()
	if(Global.get_total_item("torch")>0):
		$torch.visible = true
	else:
		$torch.visible = false
	pass # Replace with function body.

func _process(delta):
	get_node("torch/Label").set_text(str(int(timer.time_left+1)))

func _enter_tree():
	$torch.connect("pressed",self,"activeTorch")
	$clepsydre.connect("pressed",self,"activeClepsydre")
	Global.connect("items_changed",self,"setTorchText")
	Global.connect("items_changed",self,"setClepsydreText")

func _exit_tree():
	$torch.disconnect("pressed",self,"activeTorch")
	$clepsydre.disconnect("pressed",self,"activeClepsydre")
	Global.disconnect("items_changed",self,"setTorchText")
	Global.disconnect("items_changed",self,"setClepsydreText")

func activeTorch():
	if(Global.get_total_item("torch")<=0):
		return
	$BagSound.play()
	$TorchSound.play()
	Global.remove_item("torch")
	Global.torchActive = true
	$torch.disabled = true
	timer.set_wait_time( 10 )
	timer.one_shot = true
	$torch/Label.visible = true
	timer.start() #to start

func activeClepsydre():
	if(Global.get_total_item("clepsydre")<=0):
		return
	$HourSound.play()
	#$BagSound.play()
	Global.remove_item("clepsydre")
	var t = game.game_timer.time_left+20
	get_parent().get_node("AnimationPlayer").play("AddTime")
	get_parent().get_node("AnimationPlayer").seek(0)
	game.game_timer.stop()
	game.game_timer.set_wait_time(t)
	game.game_timer.start()
	
func _on_timer_timeout():
	$TorchSound.stop()
	Global.torchActive = false
	$torch.disabled = false
	$torch/Label.visible = false
	if(Global.get_total_item("torch")>0):
		$torch.visible = true
	else:
		$torch.visible = false
	
func setTorchText():
	$torch/nom.text = "Use ("+str(Global.get_total_item("torch"))+")"

func setClepsydreText():
	$clepsydre/nom.text = "Use ("+str(Global.get_total_item("clepsydre"))+")"
	if(Global.get_total_item("clepsydre")>0):
		$clepsydre.visible = true
	else:
		$clepsydre.visible = false
