extends Node2D

signal goal_reached()

var offset : Vector2 = Vector2(64,30)

var speed : int = 200

var target_tile : Vector2
var move_to_tile : bool = false

enum{TOP,DOWN,LEFT,RIGHT}
var orientation = RIGHT

func _ready():
	set_process(false)
	$Torche.visible = false

func _enter_tree():
	Global.connect("torch_changed",self,"on_torch_changed")

func _exit_tree():
	Global.disconnect("torch_changed",self,"on_torch_changed")

func _process(delta):
	if move_to_tile:
		go_to_target_tile(speed * delta)

func go_to_target_tile(distance : float):
	var distance_to_target = position.distance_to(target_tile)
	
	if distance <= distance_to_target:
		position = position.linear_interpolate(target_tile,distance / distance_to_target)
	else :
		position = target_tile
		move_to_tile = false
		if(orientation==RIGHT):
			play("idle_r")
		if(orientation==LEFT):
			play("idle_l")
		if(orientation==DOWN):
			play("idle_b")
		if(orientation==TOP):
			play("idle_t")
		emit_signal("goal_reached")

func move_to(destination : Vector2):
	target_tile = destination
	move_to_tile = true
	var a = rad2deg((destination-position).angle())
	if(a>=0 && a<90):
		play("walk_r")
		orientation = RIGHT
	if(a<0 && a>-90):
		play("walk_t")
		orientation = TOP
	if(a<-90):
		play("walk_l")
		orientation = LEFT
	if(a>90):
		play("walk_b")
		orientation = DOWN
	set_process(true)

func play(var n : String):
	if Global.torchActive:
		$AnimationPlayer.play(n+"_t")
	else:
		$AnimationPlayer.play(n)
		
func on_torch_changed():
	if(Global.torchActive):
		$Torche.visible = true
		#$TorchLight.visible = true
	else:
		$Torche.visible = false
		#$TorchLight.visible = false
	var p = "idle"
	if(move_to_tile):
		p = "walk"
	if(orientation==RIGHT):
		play(p+"_r")
	if(orientation==LEFT):
		play(p+"_l")
	if(orientation==DOWN):
		play(p+"_b")
	if(orientation==TOP):
		play(p+"_t")
