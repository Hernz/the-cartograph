extends Node2D
class_name Game

signal map_loaded(new_map)

var lvl_1_path : String =  "res://Maps/Lvl1_rng.tscn" #"res://Maps/Lvl1.tscn"
var lvl_2_path : String =  "res://Maps/Lvl2_rng.tscn" #"res://Maps/Lvl2.tscn"
var lvl_3_path : String =  "res://Maps/Lvl3_rng.tscn" #"res://Maps/Lvl3.tscn"


var game_timer : Timer

func _ready():
	$fade.visible = true
	Global.torchActive = false
	if Global.difficulty == "Easy":
		load_map(lvl_1_path)
	elif Global.difficulty == "Medium":
		load_map(lvl_2_path)
	elif Global.difficulty == "Hard":
		load_map(lvl_3_path)
	yield(self,"map_loaded")
	game_timer.start()
	game_timer.connect("timeout",find_node("EndGameScreen"),"_on_game_end")
	find_node("EndButton").connect("pressed",find_node("EndGameScreen"),"_on_game_end")

func place_player():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var current_map = find_node("CurrentMap",true,false)
	var ground_cells = current_map.get_free_cells()
	var new_pos = ground_cells[rng.randi_range(0,ground_cells.size()-1)]
	var player = find_node("Player")
	player.position = current_map.get_node("BackGround").map_to_world(new_pos)

func _process(delta):
	find_node("Label").set_text(str(int(game_timer.time_left)))

func load_map(path : String):
	get_tree().paused = true
	var new_map = load(path).instance()
	new_map.set_name("CurrentMap")
	if new_map == null:
		print("Can't find a level at this path")
		return
	var current_map = find_node("CurrentMap")
	var target_for_map = find_node("LevelContainer")
	if current_map != null:
		target_for_map.remove_child(current_map)
		current_map.queue_free()
	yield(get_tree().create_timer(0.3),"timeout")
	new_map.find_node("BackGround").hide()
	target_for_map.add_child(new_map)
	target_for_map.move_child(new_map,0)
	new_map.find_node("Objects").hide()
	print("salut")
	find_node("Hypotheses").clear()
	game_timer = new_map.find_node("Timer")
	place_player()
	emit_signal("map_loaded",new_map)
	get_tree().paused = false
	$AnimationPlayer.play("fadein")
