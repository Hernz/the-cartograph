extends Light2D


var rng = RandomNumberGenerator.new()
func _ready():
	rng.randomize()

var time = 0
var dt = 0.1
func _process(delta: float) -> void:
	if(time>=dt):
		texture_scale = rng.randf_range(1.0,1.5)
		energy = rng.randf_range(0.7, 1.1)
		time = 0
		dt = rng.randf_range(0.05,0.2)
	else:
		time += delta

