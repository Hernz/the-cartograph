shader_type canvas_item;

uniform sampler2D  n1;
uniform sampler2D  n2;
uniform sampler2D  n3;

uniform vec4 color1 : hint_color;
uniform vec4 color2 : hint_color;
uniform vec4 color3 : hint_color;

uniform float s1 : hint_range(0, 1) = 0.5 ;
uniform float s2 : hint_range(0, 1) = 0.2;

void fragment(){
	vec2 o = vec2(0,1)*TIME;
	vec4 col = texture(n1,(UV+o*0.2)*vec2(1,0.5))*texture(n2,(UV+o*0.4)*vec2(1,0.7));
	col *=  texture(n3,UV) *2.0;
	
	if(col.r>s1)
		col = color1;
	else if(col.r>s2)
		col = mix(color2,color3,1.0-UV.y);
	else
		col.a = 0.0;
	COLOR = col;
}