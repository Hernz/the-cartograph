shader_type canvas_item;

uniform int thickness = 1;
uniform vec4 color : hint_color;

void fragment(){
	vec2 ts = vec2(textureSize(TEXTURE, 0));
	float thicknessf = (float(thickness));
	vec2 pixel_size = 1.0 / ts;
	vec4 c = vec4(0,0,0,0);
	if(UV.x<=thicknessf*pixel_size.x)
		c = color;
	if(UV.x>=(ts.x-thicknessf)*pixel_size.x)
		c = color;
	if(UV.y<=thicknessf*pixel_size.y)
		c = color;
	if(UV.y>=(ts.x-thicknessf)*pixel_size.y)
		c = color;
	COLOR = c;
}