shader_type canvas_item;

uniform float thickness = 0.3;
uniform float speed = 0.2;
uniform vec4 color : hint_color;

void fragment(){
	vec4 col = texture(TEXTURE,UV);
	vec4 cadd = vec4(0);
	float vmax = (cos(TIME*speed*10.0+thickness)*0.5+0.5)*5.0;
	float vmin = (cos(TIME*speed*10.0)*0.5+0.5)*5.0;
	
	if(UV.x+UV.y*0.5<vmax&& UV.x+UV.y*0.4>vmin && col.a>0.0)
		cadd = color;
	
	COLOR = col+cadd ;
}