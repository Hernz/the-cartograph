extends Control
class_name Barre

##############################################################################
#/!\ Le nom des fils de HBox est l'index dans le tileset de Hypotheses /!\   #
##############################################################################


var selectedTextureRect : TextureRect = null
var tileset :TileSet = preload("res://Assets/Tilemap/Cave/cave_objects.tres")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if($HBox.get_child_count()>0):
		_select($HBox.get_children()[0])
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass

func _select(var icon : TextureRect):
	if(!icon):
		return
	for c in $HBox.get_children():
		c.get_node("Selecteur").visible = false
	icon.get_node("Selecteur").visible = true
	selectedTextureRect = icon

#index correspond à l'index dans le tileset de Hypotheses
func get_index_of_selected_tile():
	return tileset.find_tile_by_name(selectedTextureRect.get_name())
	
#Renvoie le nom du noeud fils du TextureRect selectionné (le nom du noeud icon)
func get_name_of_selected_tile():
	assert(selectedTextureRect)
	var res = null
	if(selectedTextureRect.get_child_count()>=2):
		if selectedTextureRect.get_children()[0].get_name()=="Selecteur":
			res = selectedTextureRect.get_children()[1]
		else:
			res = selectedTextureRect.get_children()[0]
	return res.get_name()
