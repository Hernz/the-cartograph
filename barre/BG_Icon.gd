extends TextureRect


onready var barre = get_parent().get_parent() #Barre (Ah ah ah !)


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("gui_input",self,"select")
	pass # Replace with function body.

func select(var e : InputEvent):
	if(e.is_action_pressed("ui_select")):
		barre._select(self)
