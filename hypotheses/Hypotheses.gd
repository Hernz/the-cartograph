extends TileMap
class_name Hypotheses

onready var barre : Barre = get_node("/root").find_node("Barre",true,false)
onready var ghost : TileMap = get_node("Ghost")
onready var grid : TileMap = get_node("/root").find_node("Grid",true,false)

var background : TileMap
var objects : TileMap

func _ready():
	get_node("/root/Game").connect("map_loaded",self,"_on_new_map_loaded")

func _input(event):
	if(event.is_action_pressed("ui_select")):
		var pos = get_global_mouse_position() #get_viewport().get_mouse_position()
		var tile_pos = background.world_to_map(background.to_local(pos))
		#var cell = background.get_cellv(tile_pos)
		if(grid.get_cellv(tile_pos)!=-1 && barre.get_index_of_selected_tile()!=-1): #cell!=-1 &&
			$NoteSound.play()
			set_cellv(tile_pos, barre.get_index_of_selected_tile())
			#print(barre.get_name_of_selected_tile())
	elif(event.is_action_pressed("ui_unselect")):
		var pos = get_global_mouse_position() #get_viewport().get_mouse_position()
		var tile_pos = background.world_to_map(background.to_local(pos))
		set_cellv(tile_pos,-1)
	elif event is InputEventMouseMotion:
		var pos = get_global_mouse_position() #get_viewport().get_mouse_position()
		var tile_pos = background.world_to_map(background.to_local(pos))
		#var cell = background.get_cellv(tile_pos)
		ghost.clear()
		if(grid.get_cellv(tile_pos)!=-1 && barre.get_index_of_selected_tile()!=-1):#cell!=-1 &&
			ghost.set_cellv(tile_pos, barre.get_index_of_selected_tile())

func _on_new_map_loaded(new_map):
	background = new_map.find_node("BackGround")
	objects = new_map.find_node("Objects")
	for pos in objects.get_used_cells():
		var objCellId = objects.get_cellv(pos)
		if(objCellId==tile_set.find_tile_by_name("barrel")):
			set_cellv(pos,objCellId)

func get_report() -> Dictionary:
	var res = {
		"totalTrap":0,
		"trapFound": 0,
		"totalbearTrap" : 0,
		"beartrapFound" : 0,
		"totalChest": 0,
		"chestFound": 0,
		"totalGate": 0,
		"gateFound": 0,
		"errors": 0
	}
	for pos in objects.get_used_cells():
		var objCellId = objects.get_cellv(pos)
		var hypCellId = get_cellv(pos)
		if objCellId==tile_set.find_tile_by_name("trap_object"):
			res["totalTrap"]+=1
			if objCellId==hypCellId:
				res["trapFound"]+=1
		if objCellId==tile_set.find_tile_by_name("beartrap"):
			res["totalbearTrap"]+=1
			if objCellId==hypCellId:
				res["beartrapFound"]+=1
		if(objCellId==tile_set.find_tile_by_name("chest")):
			res["totalChest"]+=1
			if objCellId==hypCellId:
				res["chestFound"]+=1
		if(objCellId==tile_set.find_tile_by_name("gate")):
			res["totalGate"]+=1
			if objCellId==hypCellId:
				res["gateFound"]+=1
	for pos in get_used_cells():
		var objCellId = objects.get_cellv(pos)
		var hypCellId = get_cellv(pos)
		if objCellId!=hypCellId:
			res["errors"]+=1
	return res
